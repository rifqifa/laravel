<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function Register(){
        return view('form');
    }

    public function Signup (Request $req) {
        $fname = $req->fname;
        $lname = $req->lname;

        return view('welcome', compact('fname', 'lname'));
    }
}
