<!DOCTYPE html>
<html>
    <head>
        <title>SanberBook</title>
    </head>

    <body>
        <div>
            <h1>Buat Account Baru!</h1>
            <h2>Sign Up Form</h2>
        </div>

        <div>
            
            <form action="/register" method="POST">
                @csrf
                <label for="First_Name">First Name</label>
                <br><br>
                <input type="text" placeholder="First Name" name ="fname" id="First_Name" required>
                <br><br>
                <label for="Last_Name" >Last Name</label>
                <br><br>
                <input type="text" placeholder="Last Name" name="lname" id="Last_Name" required>
                <br><br>

                <label>Gender:</label>
                <br><br>
                <input type="radio" name="Gender" value="Male" required>Male
                <br>
                <input type="radio" name="Gender" value="Female" required>Female
                <br><br>

                
                <label>Nationality:</label>
                <select name="nt">
                    <option value="ind">Indonesian</option>
                    <option value="sg">Singaporean</option>
                    <option value="my">Malaysian</option>
                    <option value="aus">Australian</option>
                </select>
                <br><br>

                <label>Language Spoken:</label>
                <br><br>
                <input type="checkbox" name="Language" value="ind">Bahasa Indonesia
                <br>
                <input type="checkbox" name="Language" value="eng">English
                <br>
                <input type="checkbox" name="Language" value="other">Other
                <br><br>

                <label>Bio:</label>
                <br><br>
                <textarea cols="25" row="50" placeholder="about me"></textarea>
                <br><br>

                <input type="submit" value="Sign Up">
            </form>

        </div>
    </body>
</html>